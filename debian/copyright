Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: GenomeThreader
Source: https://github.com/genometools/genomethreader
Files-Excluded: src/external
                www

Files: *
Copyright: © 2003-2019 Gordon Gremme <gordon@gremme.org>
             2003-2008 Center for Bioinformatics, University of Hamburg
License: ISC

Files: debian/*
Copyright: © 2020-2023 Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
License: ISC

Files: src/gth/bssm_param.c
Copyright: (c) 2003-2013 Gordon Gremme <gordon@gremme.org>
           (c) 2003-2005 Michael E Sparks <mespar1@iastate.edu>
           (c) 2003-2008 Center for Bioinformatics, University of Hamburg
License: ISC

Files: src/gth/bssm_param.h
Copyright: (c) 2003-2010 Gordon Gremme <gordon@gremme.org>
           (c) 2003-2005 Michael E Sparks <mespar1@iastate.edu>
           (c) 2003-2008 Center for Bioinformatics, University of Hamburg
License: ISC

Files: src/gth/bssm_param_hard_coded.h
Copyright: (c) 2003-2007 Gordon Gremme <gordon@gremme.org>
           (c) 2003      Michael E Sparks <mespar1@iastate.edu>
           (c) 2003-2007 Center for Bioinformatics, University of Hamburg
License: ISC

Files: src/gth/bssm_param_rep.h
Copyright: (c) 2003-2010 Gordon Gremme <gordon@gremme.org>
           (c) 2003-2005 Michael E Sparks <mespar1@iastate.edu>
           (c) 2003-2008 Center for Bioinformatics, University of Hamburg
License: ISC

Files: src/gth/editoperation.c
Copyright: (c) 2003-2009 Gordon Gremme <gordon@gremme.org>
           (c) 2003      Stefan Kurtz <kurtz@zbh.uni-hamburg.de>
           (c) 2003-2008 Center for Bioinformatics, University of Hamburg
License: ISC

Files: src/gth/editoperation.h
Copyright: (c) 2003-2009 Gordon Gremme <gordon@gremme.org>
           (c) 2003      Stefan Kurtz <kurtz@zbh.uni-hamburg.de>
           (c) 2003-2007 Center for Bioinformatics, University of Hamburg
License: ISC

Files: src/gth/gt_gthbssmbuild.c src/gth/gt_gthbssmprint.c src/gth/gt_gthmkbssmfiles.c
Copyright: (c) 2004-2009 Gordon Gremme <gordon@gremme.org>
           (c) 2004      Michael E Sparks <mespar1@iastate.edu>
           (c) 2004-2008 Center for Bioinformatics, University of Hamburg
License: ISC

Files: src/gth/gthalignment.c src/gth/gthalignment.h
Copyright: (c) 2003-2009 Gordon Gremme <gordon@gremme.org>
           (c) 2000-2004 Stefan Kurtz <kurtz@zbh.uni-hamburg.de>
           (c) 2003-2008 Center for Bioinformatics, University of Hamburg
License: ISC

Files: src/gth/gthprobdef.h
Copyright: (c) 2004-2007 Gordon Gremme <gordon@gremme.org>
           (c) 2004      Michael E Sparks <mespar1@iastate.edu>
           (c) 2004-2007 Center for Bioinformatics, University of Hamburg
License: ISC

License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE
