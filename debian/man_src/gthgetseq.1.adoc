# gthgetseq(1)

## NAME

gthgetseq - get FASTA sequences from GenomeThreader files

## SYNOPSIS

*gthgetseq* -getcdna | -getprotein | -getgenomic [option ...] [file ...]

## DESCRIPTION

Get FASTA sequences from GenomeThreader files containing intermediate results.
The sequences are shown on stdout.

## OPTIONS

*-getcdna*::
  get cDNA/EST sequences
                   default: no

*-getcdnacomp*::
  get complement of cDNA/EST sequences
                   default: no

*-getprotein*::
  get protein sequences
                   default: no

*-getproteincomp*::
  get complement of protein sequences
                   default: no

*-getgenomic*::
  get genomic sequences
                   default: no

*-getgenomiccomp*::
  get complement of genomic sequences
                   default: no

*-minalignmentscore*::
  set the minimum alignment score for spliced alignments to be
                   included into the set of spliced alignments
                   default: 0.00

*-maxalignmentscore*::
  set the maximum alignment score for spliced alignments to be
                   included into the set of spliced alignments
                   default: 1.00

*-mincoverage*::
  set the minimum coverage for spliced alignments to be
                   included into the set of spliced alignments
                   default: 0.00

*-maxcoverage*::
  set the maximum coverage for spliced alignments to be
                   included into the set of spliced alignments
                   default: 9999.99

*-gzip*::
  gzip compressed input file(s)
                   default: no

*-bzip2*::
  bzip2 compressed input file(s)
                   default: no

*-help*::
  display help and exit

*-version*::
  display version information and exit
