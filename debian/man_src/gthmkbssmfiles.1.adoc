# gthmkbssmfiles(1)

## NAME

gthmkbssmfiles - rite hard coded BSSM files

## SYNOPSIS

*gthmkbssmfiles* output_path

## DESCRIPTION

Write hard coded BSSM files to output_path.

## OPTIONS

*-help*::
  display help and exit

*-version*::
  display version information and exit

