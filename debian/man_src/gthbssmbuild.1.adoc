# gthbssmbuild(1)

## NAME

gthbssmbuild - build splice site model

## SYNOPSIS

*gthbssmbuild* [option ...] genomic_file cDNA_file

## DESCRIPTION

Build a BSSM file from a directory tree containing the training data.

## OPTIONS


*-bssmfile*::
  specify name of BSSM file to store parameters in
            default: undefined

*-datapath*::
  specify root of species-specific training data directory tree
            default: undefined

*-gtdonor*::
  train GT donor model
            default: no

*-gcdonor*::
  train GC donor model
            default: no

*-agacceptor*::
  train AG acceptor model
            default: no

*-gzip*::
  use gzip'ed input files
            default: no

*-help*::
  display help and exit

*-version*::
  display version information and exit
